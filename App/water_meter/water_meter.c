/*
 * water_meter.c
 *
 *  Created on: 21-Apr-2021
 *      Author: arun
 */

#include <string.h>
#include <stdlib.h>
#include "hw.h"
#include "timeServer.h"
#include "util_console.h"
#include "lora.h"
#include "water_meter.h"

/* Private variables ---------------------------------------------------------*/

/*     Status Flags     */
extern LoraFlagStatus AppProcessRequest ;

/* Timer Event   */
extern TimerEvent_t TxTimer;

#define R1 	10		/*10 K resistor R1 in voltage divider*/
#define R2 	10		/*10 K resistor R2 in voltage divider*/

/* Variables for Water Meter Calculations */
uint8_t startCalcFlag=0;
uint32_t totalLitre;
uint8_t litre_10;
uint8_t litre_100; //x1 will be set with the tens digit of the first reading
uint32_t litre_1000;  //litre_1000 is water reading in cubic meter,litre_100 is one point after decimal,litre_10 is two point after decimal.
extern unsigned char tam;

void waterMeter_Reading(sensor_t *sensor_data)
{

 sensor_data->TotalWater  = totalLitre;//*(uint32_t *)0x08080004

}

/*GPIO INIT */

void init_gpio_interrupt()
{

	GPIO_InitTypeDef initStruct = {0};
    initStruct.Mode = GPIO_MODE_IT_FALLING;
    initStruct.Pull = GPIO_PULLUP;
    initStruct.Speed = GPIO_SPEED_HIGH;

    HW_GPIO_Init(GPIOB, GPIO_PIN_8, &initStruct);
    HW_GPIO_SetIrq(GPIOB, GPIO_PIN_8, 1, switch1_interruptFunction);
    HW_GPIO_Init(GPIOB, GPIO_PIN_9, &initStruct);
  	HW_GPIO_SetIrq(GPIOB, GPIO_PIN_9, 1, switch2_interruptFunction);

  }


/*GPIO INIT */

void init_gpio_tamp_interrupt()
{
	    GPIO_InitTypeDef initStruct = {0};
	    initStruct.Mode = GPIO_MODE_IT_RISING;
	    initStruct.Pull = GPIO_PULLUP;
	    initStruct.Speed = GPIO_SPEED_HIGH;

	    HW_GPIO_Init(GPIOB, GPIO_PIN_14, &initStruct);
	    HW_GPIO_SetIrq(GPIOB, GPIO_PIN_14, 0, tamp_interruptFunction);
}

/*REED SWITCH INTERRUPT 2 */
/**
 * Created on: Oct 20, 2020
 * Last Edited: Nov 1, 2020
 * Author: Arun, Ajmi-- ICFOSS
 *
 * @brief  read the interrupt pin status
 * @param  none
 *
 **/
void switch2_interruptFunction()
{
//	GPIO_PinState tam = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_7);

	if(startCalcFlag==1)
		  {

		litre_10++;
		startCalcFlag=0;

		  };

	    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_9);
	    flowmeter();
	    GPIO_InitTypeDef initStruct = {0};
	    initStruct.Mode = GPIO_MODE_IT_FALLING;
	    initStruct.Pull = GPIO_PULLUP;
	    initStruct.Speed = GPIO_SPEED_HIGH;

	    HW_GPIO_Init(GPIOB, GPIO_PIN_8, &initStruct);
	    HW_GPIO_SetIrq(GPIOB, GPIO_PIN_8, 1, switch1_interruptFunction);

}

/*REED SWITCH INTERRUPT 1 */
/**
 * Created on: Oct 20, 2020
 * Last Edited: Nov 1, 2020
 * Author: Arun, Ajmi-- ICFOSS
 *
 * @brief  read the interrupt pin status
 * @param  none
 *
 **/

void switch1_interruptFunction() {

//GPIO_PinState tam = HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_7);
//PRINTF("TAMP = %d\n",tam);
if(startCalcFlag==0)
      {
	startCalcFlag=1;
      }
	HAL_GPIO_DeInit(GPIOB, GPIO_PIN_8);
	GPIO_InitTypeDef initStruct = {0};
	    initStruct.Mode = GPIO_MODE_IT_FALLING;
	    initStruct.Pull = GPIO_PULLUP;
	    initStruct.Speed = GPIO_SPEED_HIGH;

	    HW_GPIO_Init(GPIOB, GPIO_PIN_9, &initStruct);
	    HW_GPIO_SetIrq(GPIOB, GPIO_PIN_9, 1, switch2_interruptFunction);
}


/*WATER FLOW CALCULATION */
/**
 * Created on: Oct 21, 2020
 * Last Edited: Nov 3 , 2020
 * Author: Arun
 *
 * @brief  logical calclation of the water flow
 * @param  none
 *
 **/
void flowmeter()

    {


  if(litre_10>9)
  {
	  litre_100++;
      litre_10=0; //100 L

  }
  if(litre_100>9)
  {
	  litre_1000++;   //1000 L
	  litre_100=0;
  }

 totalLitre=((litre_1000*1000)+(litre_100*100)+(litre_10*10));
 PRINTF("totalLitre = %d\n",totalLitre);
// PRINTF("litre_10 = %d\n",litre_10);
// PRINTF("litre_100 = %d\n",litre_100);
// PRINTF("litre_1000 = %d\n",litre_1000);
    }

/*BATTERY VOLTAGE CALCULATION*/
/**
 * Created on: Oct 28, 2020
 * Last Edited: Nov 15 2020
 * Author: Arun
 *
 * @brief  read external battery voltage connected to analog channel 4 through a voltage divider
 * @param  none
 * @retval battery voltage level
 *
 **/
uint16_t batterylife()
{
	    int analogValue = 0; /*   adc reading for battery is stored in the variable  */
		float batteryVoltage = 0;
		uint16_t BatteryLevel = 0; /*    battery voltage   */

		/* enable battery voltage reading */
		enable();

		/* Read battery voltage reading */
		analogValue = HW_AdcReadChannel(ADC_CHANNEL_4);

		/* disable battery voltage reading */
		disable();

		/*battery voltage = ADC value*Vref*2/4096   --12 bit ADC with voltage divider factor of 2 */
		batteryVoltage = (analogValue * 3.3 * ((R1 + R2) / R2)) / 4096;

		/*multiplication factor of 100 to convert to int from float*/
		BatteryLevel = (uint16_t) (batteryVoltage * 100);
		PRINTF("Battery1 = %d\n\r",BatteryLevel);
		return BatteryLevel;
	}



/*TAMPERING INTERRUPT  */
/**
 * Created on: Oct 20, 2020
 * Last Edited: Nov 1, 2020
 * Author: Arun, Ajmi-- ICFOSS
 *
 * @brief  read the interrupt pin status
 * @param  none
 *
 **/

void tamp_interruptFunction()
{
	tam=1;

}

/*  watermeterInit */
/**
 * Created on: July 20, 2021
 * Last Edited: July 20, 2021
 * Author: Arun
 *
 * @brief initialize  water meter system
 * @param none
 * @retval none
 *
 **/
void watermeterGPIO_Init() {

	GPIO_InitTypeDef initStruct = { 0 };
	initStruct.Pull = GPIO_NOPULL;
	initStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	initStruct.Mode = GPIO_MODE_OUTPUT_PP;


	HW_GPIO_Init(GPIOB, GPIO_PIN_2, &initStruct);

}

/*  waterMeterInit */
/**
 * Created on: July 20, 2021
 * Last Edited: July 20, 2021
 * Author: Arun
 *
 * @brief initialize  water meter system
 * @param none
 * @retval none
 *
 **/
void watermeterInit() {

	watermeterGPIO_Init();
}

/*  enable  */
/**
 * Created on: July 20, 2021
 * Last Edited: July 20, 2021
 * Author: Arun
 *
 * @brief manual control of gpio
 * @param the gpio to be enabled
 * @retval none
 *
 **/
void enable() {

		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET); //for battery

}

/*  disable  */
/**
 * Created on: July 20, 2021
 * Last Edited: July 20, 2021
 * Author: Arun
 *
 * @brief manual control of gpio
 * @param the gpio to be disabled
 * @retval none
 *
 **/
void disable() {

		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_SET); //for battery

}



