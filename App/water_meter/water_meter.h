/*
 * water_meter.h
 *
 *  Created on: 21-Apr-2021
 *      Author: arun
 */
#ifndef __WATER_METER_H__
#define __WATER_METER_H__

/* Includes ------------------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

typedef struct
{

  int32_t TotalWater;
  bool Tamp;
  /**more may be added*/
} sensor_t;


/* Exported constants --------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
/* Exported macros -----------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
/**
 * @brief  initialises the sensor
 *
 * @note
 * @retval None
 */


/**
 * @brief  sensor  read.
 *
 * @note none
 * @retval sensor_data
 */
void waterMeter_Reading(sensor_t *sensor_data);
void init_gpio_interrupt();
void switch2_interruptFunction();
void switch1_interruptFunction();
void init_gpio_tamp_interrupt();
void tamp_interruptFunction();
void flowmeter();
void eeprom_write();
uint16_t batterylife();
void watermeterGPIO_Init();
void watermeterInit();
void enable();
void disable();
#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
