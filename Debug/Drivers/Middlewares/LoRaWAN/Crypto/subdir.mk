################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/Middlewares/LoRaWAN/Crypto/aes.c \
../Drivers/Middlewares/LoRaWAN/Crypto/cmac.c \
../Drivers/Middlewares/LoRaWAN/Crypto/soft-se.c 

OBJS += \
./Drivers/Middlewares/LoRaWAN/Crypto/aes.o \
./Drivers/Middlewares/LoRaWAN/Crypto/cmac.o \
./Drivers/Middlewares/LoRaWAN/Crypto/soft-se.o 

C_DEPS += \
./Drivers/Middlewares/LoRaWAN/Crypto/aes.d \
./Drivers/Middlewares/LoRaWAN/Crypto/cmac.d \
./Drivers/Middlewares/LoRaWAN/Crypto/soft-se.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/Middlewares/LoRaWAN/Crypto/aes.o: ../Drivers/Middlewares/LoRaWAN/Crypto/aes.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32L072xx -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -c -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../App/Core/Inc -I../App/LoRaWAN/inc -I../Drivers/BSP/CMWX1ZZABZ-0xx -I../Drivers/Middlewares/LoRaWAN/Crypto -I../Drivers/Middlewares/LoRaWAN/Mac -I../Drivers/Middlewares/LoRaWAN/Mac/region -I../Drivers/Middlewares/LoRaWAN/Phy -I../Drivers/Middlewares/LoRaWAN/Utilities -I../Drivers/BSP/B-L072Z-LRWAN1 -I../Drivers/BSP/Components/sx1276 -I../Drivers/Middlewares/LoRaWAN/Patterns/Basic -I../App/water_meter -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/Middlewares/LoRaWAN/Crypto/aes.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Drivers/Middlewares/LoRaWAN/Crypto/cmac.o: ../Drivers/Middlewares/LoRaWAN/Crypto/cmac.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32L072xx -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -c -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../App/Core/Inc -I../App/LoRaWAN/inc -I../Drivers/BSP/CMWX1ZZABZ-0xx -I../Drivers/Middlewares/LoRaWAN/Crypto -I../Drivers/Middlewares/LoRaWAN/Mac -I../Drivers/Middlewares/LoRaWAN/Mac/region -I../Drivers/Middlewares/LoRaWAN/Phy -I../Drivers/Middlewares/LoRaWAN/Utilities -I../Drivers/BSP/B-L072Z-LRWAN1 -I../Drivers/BSP/Components/sx1276 -I../Drivers/Middlewares/LoRaWAN/Patterns/Basic -I../App/water_meter -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/Middlewares/LoRaWAN/Crypto/cmac.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Drivers/Middlewares/LoRaWAN/Crypto/soft-se.o: ../Drivers/Middlewares/LoRaWAN/Crypto/soft-se.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32L072xx -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -c -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../App/Core/Inc -I../App/LoRaWAN/inc -I../Drivers/BSP/CMWX1ZZABZ-0xx -I../Drivers/Middlewares/LoRaWAN/Crypto -I../Drivers/Middlewares/LoRaWAN/Mac -I../Drivers/Middlewares/LoRaWAN/Mac/region -I../Drivers/Middlewares/LoRaWAN/Phy -I../Drivers/Middlewares/LoRaWAN/Utilities -I../Drivers/BSP/B-L072Z-LRWAN1 -I../Drivers/BSP/Components/sx1276 -I../Drivers/Middlewares/LoRaWAN/Patterns/Basic -I../App/water_meter -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/Middlewares/LoRaWAN/Crypto/soft-se.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

